
CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Installation
 * Running tests
 * Creating features and scenarios

INTRODUCTION
------------
For more background on Behat, see http://docs.behat.org

INSTALLATION
------------
This requires a Linux-based system.

You can follow the installation instructions at 
http://drupal.org/project/doobie

RUNNING TESTS
-------------
To run tests, change into the project directory and run:

bin/behat

This will cycle through all of the available features and scenarios and output
their results.

See http://docs.behat.org/guides/6.cli.html for other, fancier ways to run tests.

FEATURES AND SCENARIOS
----------------------
Human-readable features and scenarios are available in the features/ directory.
The actual code for each can be found in the 'bootstrap' directory within.

A tutorial on how to write features, scenarios, and tests can be found at http://docs.behat.org/guides/1.gherkin.html

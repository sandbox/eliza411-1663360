Feature: Get a feed of Search issues
  In order to see the Search issues Feeds
  As a user
  I should be able to see the rss feeds icon

  Scenario:
    Given that I am on the homepage
    When I follow "Issue comments"
    And I should see "Subscribe with RSS"
    When I click on the feed icon
    Then I should see the text "Issues for all projects" in the feed
    And I should see the text "Module enabling in tests is changed (for drupal 8)" in the feed
    And I should see at least "2" feed items
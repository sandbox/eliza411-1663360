<?php
#features/bootstrap/SubContext.php
/*
 * Class to define functions that are not commonly used by all features.
 * @todo Should find a way to define a SubContext based on feature we write
 * i.e 1 SubContext class for each feature
 */

use Behat\Behat\Context\BehatContext;
use Behat\Behat\Context\Step\Given;
use Behat\Behat\Context\Step\When;

class SubContext extends BehatContext
{
  var $mainContext = "";
  public function __construct(array $parameters)
  {
    // do subcontext initialization
  }

  /**
    * @Given /^I wait "([^"]*)" seconds for the slideshow to load$/
    */
  public function iWaitSecondsForTheSlideshowToLoad($seconds) {
    $this->iWaitForSeconds($seconds);
  }

  /**
   * @Given /^I should see the link "([^"]*)" at the "([^"]*)" in the right sidebar$/
   */
  public function iShouldSeeTheLinkAtTheInTheRightSidebar($link, $position) {
    $mainContext = $this->getMainContext();
    $page = $mainContext->getSession()->getPage();
    $error = 0;
    $curr_url = $mainContext->getSession()->getCurrentUrl();
    $message = "The page ".$curr_url." did not contain the specified texts";
    $nodes = $page->findAll("css", $mainContext->right_sidebar." .item-list a");
    if (sizeof($nodes)) {
      // get all the categories
      foreach ($nodes as $node) {
        $categories[] = $node->getText();
      }
      // check for firt element
      if ($position == "top") {
        if ($link != $categories[0]) {
          $error = 1;
        }
      }
      // check for last element
      elseif ($position == "bottom") {
        if($link != $categories[sizeof($categories) - 1]) {
          $error = 1;
        }
      }
      if ($error == 1) {
        $message = "The page " . $curr_url . " does not contain '" .
        $link . "' in " . $position . " position";
      }
      else {
        return true;
      }
    }
    throw new Exception($message);
  }
   /**
    * @Then /^I wait for the suggestion box to appear$/
    */
  public function iWaitForTheSuggestionBoxToAppear()
  {
    $seconds = 1;
    $this->iWaitForSeconds($seconds, "$('#edit-search-term-results').children().length > 0");
  }

  /**
   * @When /^I check my posts$/
   */
  public function iCheckMyPosts($type = 'author')
  {
    $user_found = 0;
    $main_context = $this->getMainContext();
    $main_session = $main_context->getSession();
    $page = $main_session->getPage();
    $posts_url = $main_session->getCurrentUrl();
    $fieldElements = $page->findAll('css','#tracker a');
    $auth = $main_context->getAuth();
    $username = $auth['drupal_auth']['general']['username'];
    if ($fieldElements) {
      foreach ($fieldElements as $obj_a) {
        // if username is eaqual to link label assume it as author column
        if ($username == $obj_a->getText()) {
          $user_found++;
          // go back to posts page
          $main_session->visit($posts_url);
        }else {
          // assume it as issue link and visit the link
          $link = $obj_a->getAttribute('href');
          $main_session->visit($obj_a->getAttribute('href'));
          $session = $main_context->getSession();
          $page = $session->getPage();
          // skip profile page of different user if visited by mistake
          if ($this->iShouldSeeTheText('Personal information')) {
            // go back to posts page
            $main_session->visit($posts_url);
            continue;
          }
          // search for username inside issue description page
          if ($this->iShouldSeeTheText($username)) {
            $user_found++;
            // go back to posts page
            $main_session->visit($posts_url);
          }
        }
      }
      if (!$user_found ) {
        $message = 'The user: '. $username .' is not linked to any of the posts';
      }
    }else {
      $message = "No Posts available for the user";
    }
    if(!isset($message)) {
      // all good
      return true;
    }
    throw new Exception($message);
  }

  /**
   * @Then /^(?:I|I should) see the text "([^"]*)"$/
   */
  public function iShouldSeeTheText($text) {
    // Use the Mink Extension step definition.
    return new Given("I should see text matching \"$text\"");
  }
  /**
    * @Given /^I wait "([^"]*)" seconds for the results to show$/
    */
  public function iWaitSecondsForTheResultsToShow($seconds)
  {
    $this->iWaitForSeconds($seconds, "");
  }

  /**
   * @Given /^I enter "([^"]*)" for field "([^"]*)"$/
   * This step is to be used when a label for a field is not recognized
   */
  public function iEnterForField($value, $field) {
    $field = strtolower($field);
    if ($field == "created date")
      $field = "edit-created-value";
    elseif ($field == "start date")
      $field = "edit-created-min";
    elseif ($field == "end date")
      $field = "edit-created-max";
    elseif ($field == "key modules/theme/distribution used")
      $field = "edit-field-module-0-nid-nid";
    elseif ($field == "issues")
      $field = "edit-field-issues-0-nid-nid";
    elseif ($field == "comment count")
      $field = "edit-comment-count-value";
    elseif ($field == "top level book")
      $field = "edit-title";
    elseif ($field == "comment count minimum")
      $field = "edit-comment-count-min";
    elseif ($field == "comment count maximum")
      $field = "edit-comment-count-max";
    return new Given("I fill in \"$field\" with \"$value\"");
  }

  /**
   * @When /^I check projects exist$/
   */
  public function iCheckProjectsExist()
  {
    if (!$this->iShouldSeeTheText('You have no projects')) {
      return true;
    }
    throw new Exception("Projects don't exist for the user" );
  }

 /**
     * @When /^I select "([^"]*)" from Project Type on Create Project page$/
     */
  public function iSelectFromProjectTypeOnCreateProjectPage($option)
  {
    $field = "project_type";
    switch($option) {
      case 'Modules':
        $id = 'edit-project-type-14';
        break;
      case 'Themes':
        $id = 'edit-project-type-15';
        break;
      case 'Theme engines':
        $id = 'edit-project-type-32';
        break;
      case 'Distributions':
        $id = 'edit-project-type-96';
        break;
      case 'Drupal.org projects':
        $id = 'edit-project-type-22588';
        break;
      case 'Drupal core':
        $id = 'edit-project-type-13';
        break;

    }
    $main_context = $this->getMainContext();
    $session = $main_context->getSession();
    $page = $session->getPage();
    $radio = $page->findById($id);
    $radio->click();
    $this->iWaitForSeconds(1, "");
    $this->iShouldSeeTheText('Modules categories');
  }

  /**
   * @Given /^I should download the "([^"]*)" file "([^"]*)"$/
   */
  public function iShouldDownloadTheFile($type, $filename) {
    $href = "";
    $mainContext = $this->getMainContext();
    $page = $mainContext->getSession()->getPage();
    $result = $page->findAll('css', '.views-field a');
    // get the link to download
    if (!empty($result)) {
      foreach ($result as $res) {
        if ($res->getText() == $filename) {
          $href = $res->getAttribute("href");
          break;
        }
      }
      if ($href) {
        // this will work only on Goutte as Selenium does not support this
        $mainContext->getSession()->visit($href);
        $responseHeaders = $mainContext->getSession()->getResponseHeaders();
        if ((int) $responseHeaders['Content-Length'][0] > 10000) {
          if ($type != "tar" || $type != "zip" ||
           $responseHeaders['Content-Type'] != "application/x-gzip" ||
           $responseHeaders['Content-Type'] == "application/zip") {
            throw new Exception("The file '" . $filename. "' was not downloaded");
          }
        }
        else {
          throw new Exception("The file '" . $filename. "' was not downloaded");
        }
      }
      else {
        throw new Exception("The link '" . $filename. "' was not found on the page");
      }
    }
    else {
      throw new Exception("The link '" . $filename. "' was not found on the page");
    }
  }

  /**
   * @When /^I select "([^"]*)" from field "([^"]*)"$/
   */
  public function iSelectFromField($value, $field) {
    $field = strtolower($field);
    if ($field == 'change node created')
      $field = 'created_op';
    elseif ($field == 'comment count')
      $field = 'edit-comment-count-op';
    elseif ($field == 'top level book')
      $field = 'edit-title-op';
    $mainContext = $this->getMainContext();
    $page = $mainContext->getSession()->getPage();
    $page->selectFieldOption($field, trim($value));
    if (empty($page))
      throw new Exception("Unable to select the text");
  }

  /**
   * @Given /^I should see "([^"]*)" under "([^"]*)"$/
   */
  public function iShouldSeeUnder($text, $column) {
    $result = $this->checkTextInColumn($text, $column, 1);
    if ($result)
      throw new Exception($result);
  }

  /**
   * @Given /^I should not see "([^"]*)" under "([^"]*)"$/
   */
  public function iShouldNotSeeUnder($text, $column) {
    $result = $this->checkTextInColumn($text, $column, 0);
    if ($result)
      throw new Exception($result);
  }

  /**
   * Function to check whether a particular text is present in the column or not
   * @param $text String The text to search for
   * @param $column String The column in which the search has to be performed
   * @param $flag Int A flag where 1 = Should see in all cols, 0 = should not see
   */
  private function checkTextInColumn($text, $column, $flag) {
    $message = "";
    $class = "";
    $check = FALSE;
    $mainContext = $this->getMainContext();
    $page = $mainContext->getSession()->getPage();
    // get the class name of the column
    $result = $page->findAll('css', '.view table.views-table tr th');
    if (!empty($result)) {
      foreach ($result as $res) {
        if ($res->getText() == $column) {
          $class = $res->getAttribute('class');
          // class will be like 'views-field views-field-status'
          $temp = explode(" ", $class);
          $class = $temp[1];
          break;
        }
      }
      if ($class) {
        // get the column value of each row
        $result = $page->findAll('css', '.view table.views-table tr td.'.$class);
        if (!empty($result)) {
          $text = strtolower($text);
          foreach ($result as $res) {
            $colText = strtolower($res->getText());
            // flag = 1 => The part of the text should be found in every row of the specified column
            if ($flag) {
              if (strpos($colText, $text) === FALSE) {
                $check = TRUE;
                break;
              }
            }
            // flag = 0 => The part of the text should not be found in any row of the specified column
            else {
              if (strpos($colText, $text) !== FALSE) {
                $check = TRUE;
                break;
              }
            }
          }
          if ($check) {
            if ($flag)
              $message = "The text '" . $text . "' was not found in all the rows of the column '" . $column . "'";
            else
              $message = "The text '" . $text . "' was found in atleast one row of the column '" . $column . "'";
          }
        }
        else {
          $message = "The column " . $column . " was not found in the page";
        }
      }
      else {
        $message = "The column " . $column . " was not found in the page";
      }
    }
    else {
      $message = "The column " . $column . " was not found in the page";
    }
    return $message;
  }

  /**
  * @Given /^I wait for "([^"]*)" seconds$/
  */
  public function iWaitForSeconds($seconds, $condition = "") {
    $milliseconds = (int) ($seconds * 1000);
    $this->getMainContext()->getSession()->wait($milliseconds, $condition);
  }

  /**
  * @Given /^I upload the image "([^"]*)"$/
  */
  public function iUploadTheImage($arg1)
  {
    // TODO
    throw new PendingException();
  }

  /**
  * @When /^I preview the project$/
  */
  public function iPreviewTheProject()
  {
     // TODO
    throw new Exception();
  }

  /**
  * @Given /^I fill in Project Description with "([^"]*)"$/
  */
  public function iFillInProjectDescriptionWith($description)
  {
    $this->getMainContext()->getSession()->getPage()->findById('edit-body')->setValue($description);
  }

  /**
  * @When /^I click on a case study image$/
  */
  public function iClickOnACaseStudyImage() {
    $page = $this->getSession()->getPage();
    $result = $page->find('css', '.view-content .col-first a');
    if (empty($result)) {
      throw new Exception("This page does not have any case study");
    }
    $result->click();
  }
}
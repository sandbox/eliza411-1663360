<?php
use Behat\Symfony2Extension\Context\KernelAwareInterface;
use Behat\MinkExtension\Context\MinkContext;
use Behat\Behat\Context\ClosuredContextInterface;
use Behat\Behat\Context\TranslatedContextInterface;
use Behat\Behat\Context\BehatContext;
use Behat\Behat\Exception\PendingException;
use Behat\Gherkin\Node\PyStringNode;
use Behat\Gherkin\Node\TableNode;
use Behat\Mink\Driver\GoutteDriver;
use Behat\Behat\Context\Step\Then;
use Behat\Behat\Context\Step\When;
use Behat\Behat\Context\Step\Given;


class FeatureContext extends MinkContext
{
  /**
   * Initializes context.
   *
   * Every scenario gets its own context object.
   *
   * @param array $parameters.
   *   Context parameters (set them up through behat.yml).
   */
  /* Store basic authentication */
  private $basic_auth = array();
  /* Store various drupal user data */
  private $drupal_auth = array();
  /* Store right sidebar id */
  public $right_sidebar = "";
  /* Store rss feed xml content */
  private $xmlContent = "";
  /* Store project value */
  private $project_value = '';
  /* Store home bottom right*/
  private $home_bottom_right = '';

  public function __construct(array $parameters) {
    $this->right_sidebar = $parameters['right_sidebar'];
    $this->home_bottom_right = $parameters['home_bottom_right'];
    if (isset($parameters['basic_auth']))
      $this->basic_auth = $parameters['basic_auth'];
    if (isset($parameters['drupal_auth']))
      $this->drupal_auth = $parameters['drupal_auth'];
    $this->useContext('subcontext_alias', new SubContext($parameters));
  }

  /** @BeforeFeature */
  public static function prepareForTheFeature() {
  // clean database or do other preparation stuff
  }

  /** @BeforeScenario */
  public function prepareForTheScenario() {
    // in yml set username and password parameters
    if (!empty($this->basic_auth['username']) && !empty($this->basic_auth['password'])) {
      if ($this->getSession()->getDriver() instanceof GoutteDriver) {
        $this->getSession()->setBasicAuth($this->basic_auth['username'], $this->basic_auth['password']);
      }
    }
  }

  /**
   * Override MinkContext::locatePath() to work around Selenium not supporting
   * basic auth.
   */
  protected function locatePath($path) {
    $driver = $this->getSession()->getDriver();
    if ($driver instanceof Behat\Mink\Driver\Selenium2Driver && isset($this->basic_auth)) {
      // Add basic auth params to the base url. Format:http://user:pass@base-url. Firefox only.
      $startUrl = rtrim($this->getMinkParameter('base_url'), '/') . '/';
      $startUrl = str_replace('://', '://' . $this->basic_auth['username'] .
      ':' . $this->basic_auth['password'] . '@', $startUrl);
      return 0 !== strpos($path, 'http') ? $startUrl . ltrim($path, '/') : $path;
    }
    else {
      return parent::locatePath($path);
    }
  }

  /**
   * @Then /^I should see the heading "([^"]*)"$/
   */
  public function iShouldSeeTheHeading($headingname) {
  $element = $this->getSession()->getPage();
    foreach (array('h1', 'h2', 'h3', 'h4', 'h5', 'h6') as $heading) {
      $results = $element->findAll('css', $heading);
      foreach ($results as $result) {
        if ($result->getText() == $headingname) {
          return;
        }
      }
    }
    throw new Exception("The text " . $headingname . " was not found in any
    heading " . $this->getSession()->getCurrentUrl());
  }

  /**
   * @Then /^I login to the site$/
   */
  public function iLoginToTheSite($username = "", $password = "") {
    // by default, use the general username and password
    if ($username == "")
      $username = $this->drupal_auth['general']['username'];
    if ($password == "")
      $password = $this->drupal_auth['general']['password'];
    // before login, check if he is already logged in
    $element = $this->getSession()->getPage();
    $result = $element->findLink("Log out");
    if (empty($result)) {
      return array(
        new When('I follow "Log in / Register"'),
        new Then(sprintf('I fill in "%s" for "%s"', $username, 'Username')),
        new Then(sprintf('I fill in "%s" for "%s"', $password, 'Password')),
        new Then('I press "Log in"'),
      );
    }
  }

  /**
   * @Then /^I should see the link "([^"]*)"$/
   */
  public function iShouldSeeTheLink($linkname) {
    $element = $this->getSession()->getPage();
    $result = $element->findLink($linkname);
    if (empty($result)) {
      throw new Exception("No link to " . $linkname . " on " .
      $this->getSession()->getCurrentUrl());
    }
  }

  /**
   * @Then /^I should not see the link "([^"]*)"$/
   */
  public function iShouldNotSeeTheLink($linkname) {
    $element = $this->getSession()->getPage();
    $result = $element->findLink($linkname);
    if ($result) {
      throw new Exception("The link " . $linkname . " was present on " .
      $this->getSession()->getCurrentUrl() . " and was not supposed to be.");
    }
  }

  /**
   * @Then /^I should see "([^"]*)" links on the right sidebar$/
   */
  public function iShouldSeeLinksOnTheRightSidebar($count) {
    $page = $this->getSession()->getPage();
    $nodes = $page->findAll("css", $this->right_sidebar." .item-list a");
    if (sizeof($nodes) == $count) return true;
      throw new Exception('Found ' . sizeof($nodes) . ' links instead of ' .
      $count . ' links on the right sidebar');
  }

  /**
   * @Then /^I should see the project$/
   */
  public function iShouldSeeTheProject() {
    $element = $this->getSession()->getPage();
    $result = $element->hasContent($this->project);
    if ($result === FALSE) {
      throw new Exception("The text " . $this->project . " was not found " . $session->getCurrentUrl());
    }
  }

  /**
   * @Given /^I fill in searchBox with "([^"]*)"$/
   */
  public function iFillInSearchboxWith($input)
  {
    $this->fillField("projects",$input);
  }

  /**
   * @Given /^I select "([^"]*)" from the suggestion "([^"]*)"$/
   */
  public function iSelectFromTheSuggestion($value, $field)  {
    $this->project_value = $value;
    return new Given("I fill in \"$field\" with \"$value\"");
  }

  /**
    * @When /^I press search to filter$/
    */
  public function iPressSearchToFilter()
   {
    $button = 'edit-submit-project-issue-all-projects';
    $element = $this->getSession()->getPage();
    $element->fillField('Project', $this->project_value);
    //$submit = $element->findById('edit-submit-project-issue-all-projects');
    $submit = $element->findButton($button);
    if (empty($submit)) {
      throw new Exception('No submit button at ' . $this->getSession()->getCurrentUrl());
    }
    $element->pressButton($button);
  }

  /**
    * @When /^I press "([^"]*)" to filter$/
    * TODO this should work generically to exclude the sitewide search
    */
  public function iPressToFilter($arg1) {
    $element = $this->getSession()->getPage();
    $submit = $element->findById('edit-submit-project-issue-search-all');
    if (empty($submit)) {
      throw new Exception('No submit button at ' . $session->getCurrentUrl());
    }
    
    /*if(!($submit->click())) {
      throw new Exception('No Click happened at ' . $this->getSession()->getCurrentUrl());
    }*/
    $element->pressButton('edit-submit-project-issue-search-all');
  }

  /**
   * @Given /^I should see at least "([^"]*)" records$/
   */
  public function iShouldSeeAtLeastRecords($count) {
    $page = $this->getSession()->getPage();
    // counts the number of rows in the view table
    $records = $this->getViewDisplayRows($page);
    if (!$records || sizeof($records) < $count) {
        throw new Exception("The page (" . $this->getSession()->getCurrentUrl() .
         ") has less than " . $count . " records");
    }
  }

  /**
   * @When /^I click the table heading "([^"]*)"$/
   */
  public function iClickTheTableHeading($column) {
    // make sure we click on the table heading and not any other link
    $count = 0;
    $page = $this->getSession()->getPage();
    // all table headings of a view have this class - view -> views-table -> th
    $heading = $page->findAll('css', '.view table.views-table th a');
    if (sizeof($heading)) {
      foreach ($heading as $text) {
        if ($text->getText() == $column) {
          $count++;
          $href = $text->getAttribute("href");
          $this->getSession()->visit($href);
          break;
        }
      }
      if ($count == 0) {
        throw new Exception("The page does not have a table with the
         heading '" . $column . "'");
      }
    }
    else {
      throw new Exception("The page has no table headings");
    }
  }

  /**
   * @Then /^I should see "([^"]*)" sorted in "([^"]*)" order$/
   */
  public function iShouldSeeSortedInOrder($column, $order)
  {
    $column_class = "";
    $count = 0;
    $date = FALSE;
    $page = $this->getSession()->getPage();
    $heading = $page->findAll('css', '.view table.views-table th');
    foreach ($heading as $text) {
      if ($text->getText() == $column) {
        $count = 1;
        $class = $text->getAttribute("class");
        $temp = explode(" ", $class);
        $column_class = $temp[1];
        break;
      }
    }
    if ($count == 0) {
      throw new Exception("The page does not have a table with column '" . $column . "'");
    }
    $count = 0;
    $items = $page->findAll('css', '.view table.views-table tr td.'.$column_class);
    // make sure we have the data
    if (sizeof($items)) {
      // put all items in an array
      $loop = 1;
      //date_default_timezone_set ("UTC");
      foreach ($items as $item) {
        $text = $item->getText();
        if ($loop == 1) {
          // check if the text is date field
          if ($this->isStringDate($text)) {
            $date = TRUE;
				  }
        }
        if ($date) {
          $orig_arr[] = $this->isStringDate($text);
        }
        else {
          $orig_arr[] = $text;
        }
        $loop = 2;
      }
      // create a temp array for sorting and comparing
      $temp_arr = $orig_arr;
      // sort
      if ($order == "ascending") {
        if ($date) {
          sort($temp_arr, SORT_NUMERIC);
        }
        else {
          sort($temp_arr);
        }
      }
      elseif ($order == "descending") {
        if ($date) {
          rsort($temp_arr, SORT_NUMERIC);
        }
        else {
          rsort($temp_arr);
        }
      }
      // after sorting, compare each index value of temp array & original array
      for ($i = 0; $i < sizeof($temp_arr); $i++) {
        if ($temp_arr[$i] == $orig_arr[$i]) {
          $count++;
        }
      }
      // if all indexs match, then count will be same as array size
      if ($count == sizeof($temp_arr)) {
       return true;
      }
      else {
        throw new Exception("The column '" . $column . "' is not sorted in " . $order . " order");
      }
    }
    else {
      throw new Exception("The column '" . $column . "' is not sorted in " . $order . " order");
    }
  }

  /**
   * Function to check whether the given string is a date or not
   * @param $string String The string to be checked for
   * @return $return String/Bool - Return timestamp if it is date, false otherwise
   */
  public function isStringDate($string) {
    $return = "";;
  	$string = trim($string);
  	if ($string) {
  	  $time = strtotime($string);
  	  if ($time === FALSE) {
  	    $return = FALSE;
  	  }
  	  elseif(is_numeric($time) && strlen($time) == 10) {
  	    return $time;
  	  }
  	  else {
  	    $return = FALSE;
  	  }
  	}
  	else {
      $return = FALSE;
    }
    return $return;
  }

  /**
   * @Given /^I should not see the following <texts>$/
   */
  public function iShouldNotSeeTheFollowingTexts(TableNode $table) {
    $page = $this->getSession()->getPage();
    $table = $table->getHash();
    foreach ($table as $key => $value) {
      $text = $table[$key]['texts'];
      if(!$page->hasContent($text) === FALSE) {
        throw new Exception("The text '" . $text . "' was found");
      }
    }
  }

  /**
   * @Given /^I should see the following <texts>$/
   */
  public function iShouldSeeTheFollowingTexts(TableNode $table) {
    $page = $this->getSession()->getPage();
    $table = $table->getHash();
    foreach ($table as $key => $value) {
      $text = $table[$key]['texts'];
      if($page->hasContent($text) === FALSE) {
        throw new Exception("The text '" . $text . "' was not found");
      }
    }
  }

  /**
  * @Given /^I should see the following <links>$/
  */
  public function iShouldSeeTheFollowingLinks(TableNode $table) {
    $page = $this->getSession()->getPage();
    $table = $table->getHash();
    foreach ($table as $key => $value) {
      $link = $table[$key]['links'];
      $result = $page->findLink($link);
      if(empty($result)) {
        throw new Exception("The link '" . $link . "' was not found");
      }
    }
  }

  /**
   * @Given /^I should not see the following <links>$/
   */
  public function iShouldNotSeeTheFollowingLinks(TableNode $table) {
    $page = $this->getSession()->getPage();
    $table = $table->getHash();
    foreach ($table as $key => $value) {
      $link = $table[$key]['links'];
	    $result = $page->findLink($link);
	    if(!empty($result)) {
	      throw new Exception("The link '" . $link . "' was found");
	    }
    }
  }

  /**
   * @When /^I click on page "([^"]*)"$/
   */
  public function iClickOnPage($pager) {
    $class = "";
    $page = $this->getSession()->getPage();
    $result = $page->findAll('css', '.pager .pager-item a');
    foreach ($result as $temp) {
	    if (trim($temp->getText()) == trim($pager)) {
	      $href = $temp->getAttribute("href");
        $this->getSession()->visit($href);
		    return;
	    }
	  }
    // make sure we look at pager links only
    if ($pager == "first" || $pager == "previous" || $pager == "next" || $pager == "last") {
      $class = '.pager .pager-' . $pager . ' a';
    }
    else {
      throw new Exception("The page '" . $pager . "' was not found");
    }
    $result = $page->find('css', $class);
    $href = $result->getAttribute("href");
    $this->getSession()->visit($href);
  }

  /**
   * Function to get the array of records from the current view listing
   * @param $element Object The element to search within
   * @return $result Array An array of items
  */
  private function getViewDisplayRows($element) {
    $classes = array(
      '.view table.views-table tr',
      '.view table.views-view-grid tr td',
      '.view div.views-row',
    );
    foreach ($classes as $class) {
      $result = $element->findAll('css', $class);
      if (!empty($result)) {
        return $result;
      }
    }
  }

  /**
   * @When /^I click on the feed icon$/
   * Works only with Goutte as ResponseHeaders are not supported by Selenium
   */
  public function iClickOnTheFeedIcon() {
    $page = $this->getSession()->getPage();
    $result = $page->find('css', '.feed-icon');
    if (empty($result)) {
      throw new Exception("This page does not have a feed icon");
    }
    $result->click();
    //use response headers to make sure we got the xml data and not html
    $responseHeaders = $this->getSession()->getResponseHeaders();
    if (strpos($responseHeaders['Content-Type'], "application/rss+xml") === FALSE) {
      throw new Exception("This page '" . $this->getSession()->getCurrentUrl() .
       "' does not provide xml data");
    }
    // Use goutedriver get content to get the complete xml data and store it
    //temporarily in a variable for use by function iShouldSeeTheTextInTheFeed()
    $this->xmlContent =
     $this->getSession()->getDriver()->getClient()->getResponse()->getContent();
  }

  /**
  * @When /^I select the following <fields> with <values>$/
  */
  public function iSelectTheFollowingFieldsWithValues(TableNode $table)
  {
    $multiple = true;
    $page = $this->getSession()->getPage();
    $table = $table->getHash();
    foreach ($table as $key => $value) {
    // find the select element
      $select = $page->find('named', array('select', $table[$key]['fields']));
      $multiple = $select->getAttribute('multiple') ? true : false;
      $page->selectFieldOption($table[$key]['fields'], $table[$key]['values']);
    }
  }

  /**
   * @Given /^I upload the following "([^"]*)" <files>$/
   */
  public function iUploadTheFollowingFiles($type, TableNode $files)
  {
    // Multiple file upload:
    // update the below 'if' if this function needs to be reused
    switch ($type) {
      // for Create Project image upload
      case 'project image':
        $addmore_id = 'edit-field-project-images-field-project-images-add-more';
        // upload field id
        $filefield_id 	= 'edit-field-project-images-{index}-upload';
        // upload button id
        $uploadbutton_id 	= 'edit-field-project-images-{index}-filefield-upload';
        // upload response id
        $responsebox_id	= 'edit-field-project-images-{index}-data-description';
        // upload set wrapper
        $wrapperbox_id 	= 'edit-field-project-images-{index}-ahah-wrapper';
        // parameters to be filled in after upload finishes
        $arr_postupload_params = array(
          // in description
          'description' => 'edit-field-project-images-{index}-data-description',
          // al tag
          'alt text' => 'edit-field-project-images-{index}-data-alt',
        );
        break;
      // for Create Case Study image upload
      case 'case study image':
        $addmore_id = 'edit-field-images-field-images-add-more';
        // upload field id
        $filefield_id 	= 'edit-field-images-{index}-upload';
        // upload button id
        $uploadbutton_id 	= 'edit-field-images-{index}-filefield-upload';
        // upload response id
        $responsebox_id	= 'edit-field-images-{index}-data-description';
        // upload set wrapper
        $wrapperbox_id 	= 'edit-field-images-{index}-ahah-wrapper';
        // parameters to be filled in after upload finishes
        $arr_postupload_params = array(
          // in description
          'description' => 'edit-field-images-{index}-data-description',
          // al tag
          'alt text' => 'edit-field-images-{index}-data-alt',
          // title
          'title' => 'edit-field-images-{index}-data-title',
        );
        break;
      default:
        throw new Exception('Type of files to be uploaded is not specified/correct. Eg: \'I upload the following "project image" <files>\'');
        break;
    }
    $session = $this->getSession();
    $page = $session->getPage();
    $files = $files->getHash();
    $total_files = count($files);
    $subcontext = $this->getSubcontext('subcontext_alias');
    $sele_handler = $session->getSelectorsHandler();

    // 'add more' button
    $add_more = $page->findById($addmore_id);
    $upload = 0;

    if ($total_files > 0) {
      // wait
      $subcontext->iWaitForSeconds(2);
      // loop through files and upload
      for($i=0; $i < $total_files; $i++) {
        // find newly inserted file and attach local file
        $file_id = str_replace('{index}', $i, $filefield_id);
        $file = $this->getSession()->getPage()->findById($file_id);
        //add more items
        if (!is_object($file)) {
          $subcontext->iWaitForSeconds(2);
          $wrapper_id = str_replace('{index}', $i, $wrapperbox_id);
          $add_more->click();
          $subcontext->iWaitForSeconds(10, "typeof($('#". $wrapper_id ."').html()) != 'undefined'");
          $subcontext->iWaitForSeconds(2);
          $file = $this->getSession()->getPage()->findById($file_id);
        }
        // attach again
        $file->attachFile(getcwd() . '/files/' . $files[$i]['files']);
        // find upload button and click
        $button_id = str_replace( '{index}', $i, $uploadbutton_id);
        $submit = $this->getSession()->getPage()->findById($button_id);
        $submit->click();
        // wait for upload to finish: will wait untill the upload completes OR 300 seconds
        $box_id = str_replace('{index}', $i, $responsebox_id);
        $subcontext->iWaitForSeconds(300, "typeof($('#". $box_id . "').val()) != 'undefined'");

        // process post upload parameters
        if (!empty($arr_postupload_params)) {
          foreach ($arr_postupload_params as $param => $field_id) {
            if (isset($files[$i][$param]) && !empty($files[$i][$param])) {
              $field_id = str_replace('{index}', $i, $field_id);
              $this->getSession()->getPage()->findById($field_id)->setValue($files[$i][$param]);
            }
          }
        }
        // mark as done
        $upload++;
      }
    }
    if (!$upload)
      throw new Exception('Upload failed');
  }

  /**
   * @Then /^I should see the text "([^"]*)" in the feed$/
   */
  public function iShouldSeeTheTextInTheFeed($text) {
    $xmlString = trim($this->xmlContent);
    if ($xmlString) {
      if (strpos($xmlString, trim($text)) === FALSE) {
        throw new Exception("The text '" . $text . "' was not found in the
         xml feed");
      }
    }
    else {
      throw new Exception("No xml data found");
    }
  }

  /**
   * @When /^I search sitewide for "([^"]*)"$/
   */
  public function iSearchFor($searchterm) {
    $element = $this->getSession()->getPage();
    $element->fillField('edit-search-theme-form-1', $searchterm);
    $submit = $element->findById('search-theme-form-submit');
    if (empty($submit)) {
      throw new Exception('No submit button at ' . $session->getCurrentUrl());
    }
    $submit->click();
  }

  /**
  * @Given /^(?:|that )I am on the homepage$/
  */
  public function iAmOnTheHomepage() {
    $this->getSession()->visit($this->locatePath('/'));
  }

  /**
   * @Given /^I should see at least "([^"]*)" feed items$/
   */
  public function iShouldSeeAtLeastFeedItems($count) {
    $count = (int) $count;
    $xmlString = trim($this->xmlContent);
    if ($xmlString) {
      $match = preg_match_all("/<item>/", $xmlString, $matches);
      // checks whether $count items were present in the xml feed or not
      // if count > 0, then match should be >= count
      if ($count) {
        if ($match < $count) {
          throw new Exception('The feed contains less than ' . $count .
           ' feed items');
        }
      }
      // if count = 0, then no feeds should be found
      elseif ($match > 0) {
        throw new Exception('The feed contains more than ' . $count .
         ' feed items');
      }
    }
  }

  /**
  * @When /^I select <option> from "([^"]*)" results will contain <text>$/
  */
  public function iSelectOptionFromResultsWillContainText($select, TableNode $table)
  {
    if (!empty($table)) {
      $arr_return  = array();
      $table = $table->getHash();
      // loop through page
      for ($i = 0,$count = count($table); $i < $count; $i++) {
        if (!empty($table[$i]['option']) && !empty($table[$i]['text']) ) {
          $arr_return[] = new When("I select ". $table[$i]['option'] ." from \"" . $select ."\"");
          $arr_return[] = new Then("I should see " . $table[$i]['text']);
        }
      }

	  return $arr_return;
	}else
		throw new Exception("No options/texts specified");
  }

  /**
  * @Given /^I am logged in as "([^"]*)"$/
  */
  public function iAmLoggedInAs($userName) {
    // login using git user or site user or vetter git user as configured in yml
    if (empty($this->drupal_auth[$userName])) {
      throw new Exception('The user "' . $userName . '" does not exist. Please configure the same.');
    }
    $password = $this->drupal_auth[$userName];
    return $this->iLoginToTheSite($userName, $password);
  }
  /**
   * Function to check whether the links exists under the news/specific tab
   * @param $tab String The tab to be selected for
   * @param $count counts the number of links exists
   * @Then /^I should see atleast "([^"]*)" links under tab "([^"]*)"$/
   */
  public function iShouldSeeAtleastLinksUnderTab($count, $tab) {
    $page = $this->getSession()->getPage();
    $tab = strtolower($tab);
    switch($tab) {
      case 'news':
        $id = '#fragment-1';
        break;
      case 'docs updates':
        $id = '#fragment-2';
        break;
      case 'forum posts':
        $id = '#fragment-3';
        break;
      case 'commits':
        $id = '#fragment-4';
        break;
      default:
        throw new Exception('The tab "' . ucfirst($tab) . '" was not found on the page');
        }
      $nodes = $page->findAll("css", $this->home_bottom_right." ".$id." a");
      if (sizeof($nodes) == $count) return true;
      throw new Exception('Found ' . sizeof($nodes) . ' links instead of ' .
      $count . ' links on the home bottom right');
  }

  /**
  * @Then /^I should see the following <subcategories> under "([^"]*)"$/
  */
  public function iShouldSeeTheFollowingSubcategoriesUnder($category, TableNode $table)
  {
    // find grid container
    $page = $this->getSession()->getPage();
    $grids = $page->findAll('css', 'div.grid-2');
    if (!empty($grids)) {
      $table = $table->getHash();
      $arr_subcats = array();
      $arr_visiblecats = array();
      if(!empty($table)) {
        foreach($table as $subcat) {
          $arr_subcats[] = $subcat['subcategories'];
        }
        // loop through the grid to identify appropriate DIV
        foreach ( $grids as $grid) {
          // check main category
          if (is_object($h3 = $grid->find('css', 'h3')) &&  $h3->getText() == $category) {
            // find sub-category links
            $links = $grid->findAll('css', 'ul > li > a');
            if (!empty($links)) {
              //$visible = false;
              foreach($links as $a) {
                // if visible
                if (!('display: none;' == $a->getParent()->getAttribute('style'))) {
                  // remove count with parenthasis
                  if($text = trim(preg_replace('~\(.*?\)~', "", $a->getText()))) {
                    $arr_visiblecats[] = $text;
                  }
                }
              }
            }
            break;
          }
        }
        //check presence of given subcategories in visible subcategories
        if (count($arr_np = array_diff($arr_subcats, $arr_visiblecats))) {
          $catcount = count($arr_np);
          throw new Exception('The subcategor' . ($catcount == 1 ? 'y' : 'ies') . ': "' . ($np = implode('", "', $arr_np)).'" cannot be found.');
        }
      }else {
      throw new Exception('Subcategories are not given.');
      }
    }else {
      throw new Exception('Subcategories are not given.');
    }
  }

  /**
  * @Then /^I should not see the following <subcategories> under "([^"]*)"$/
  */
  public function iShouldNotSeeTheFollowingSubcategoriesUnder($category, TableNode $table)
  {
    // find grid container
    $page = $this->getSession()->getPage();
    $grids = $page->findAll('css', 'div.grid-2');
    if (!empty($grids)) {
      $table = $table->getHash();
      $arr_subcats = array();
      $arr_hiddencats = array();
      if(!empty($table)) {
        foreach($table as $subcat) {
          $arr_subcats[] = $subcat['subcategories'];
        }

        foreach ( $grids as $grid) {
          // check main category
          if (is_object($h3 = $grid->find('css', 'h3')) &&  $h3->getText() == $category) {
            // find sub-category links
            $links = $grid->findAll('css', 'ul > li > a');
            if (!empty($links)) {
              foreach($links as $a) {
                // check the links are hidden
                if (('display: none;' == $a->getParent()->getAttribute('style'))) {
                // remove count with parenthasis
                  if($text = trim(preg_replace('~\(.*?\)~', "", $a->getText()))) {
                    $arr_hiddencats[] = $text;
                  }
                }
              }
            }
            break;
          }
        }
        //check presence of given subcategories in hidden subcategories
        if (count($arr_np = array_diff($arr_subcats, $arr_hiddencats))) {
          $catcount = count($arr_np);
          throw new Exception('The subcategor' . ($catcount == 1 ? 'y' : 'ies') . ': "' . ($np = implode('", "', $arr_np)).'" ' .($catcount == 1 ? 'is' : 'are') . ' present on the page.');
        }
      }else {
      throw new Exception('Subcategories are not given.');
      }
    }else {
      throw new Exception('Subcategories are not given.');
    }
  }

  /**
   * @Then /^I expand the category "([^"]*)"$/
   */
  public function iExpandTheCategory($category)
  {
    // find grid container
    $expanded = 0;
    $category_found = 0;
    $page = $this->getSession()->getPage();
    $grids = $page->findAll('css', 'div.grid-2');
    if (!empty($grids)) {
      foreach ( $grids as $grid) {
        // check main category
        if (is_object($h3 = $grid->find('css', 'h3')) &&  $h3->getText() == $category) {
          $category_found++;
          // find sub-category links
          $links = $grid->findAll('css', 'ul > li > a');
          if (!empty($links)) {
            foreach ($links as $a) {
              // find show more link to expand
              if ($a->getText() == 'Show more') {
                $a->click();
                $expanded++;
                break;
              }
            }
          }
        }
      }
    }
    if (!$category_found) {
      throw new Exception('The category:"' . $category .  '" cannot be found.');
    }
    if (!$expanded) {
      throw new Exception('The category: "' . $category. ' cannot be expanded');
    }
  }

  /**
   * @Then /^I collapse the category "([^"]*)"$/
   */
  public function iCollapseTheCategory($category)
  {
    // find grid container
    $collapsed = 0;
    $category_found = 0;
    $page = $this->getSession()->getPage();
    $grids = $page->findAll('css', 'div.grid-2');
    if (!empty($grids)) {
      foreach ( $grids as $grid) {
        // check main category
        if (is_object($h3 = $grid->find('css', 'h3')) &&  $h3->getText() == $category) {
          $category_found++;
          // find sub-category links
          $links = $grid->findAll('css', 'ul > li > a');
          if (!empty($links)) {
            foreach ($links as $a) {
              // find Show fewer link to collapse
              if ($a->getText() == 'Show fewer') {
                $a->click();
                $collapsed++;
                break;
              }
            }
          }
        }
      }
    }
    if (!$category_found) {
      throw new Exception('The category:"' . $category .  '" cannot be found.');
    }
    if (!$collapsed) {
      throw new Exception('The category: "' . $category. ' cannot be collapsed');
    }
  }

  /**
  * Return Auth parameters
  */
  public function getAuth() {
    // access auth params
    return array( 'basic_auth' => $this->basic_auth, 'drupal_auth' => $this->drupal_auth );
  }
  
  /**
  * @Then /^I should see the submitted user "([^"]*)"$/
  */
  public function iShouldSeeTheSubmittedUser($submUser) {
    $page = $this->getSession()->getPage();
    $result = $page->find('css', '.node .submitted a');
    if(!empty($result)) {
      $findUser = $result->getText('link');
    if(trim($findUser) == trim($submUser)) {
      return;
      }
      else {
      throw new Exception('The user "' . $submUser .  '"  was not the submitted user for this issue.');
    }
   } 
	}
  
  /**
   * @Then /^I should see at least "([^"]*)" blocks$/
  */
  public function iShouldSeeAtLeastBlocks($count)
  {
    // find dives with the class 'homebox-portlet' inside #homebox div
    $boxes = $this->getSession()->getPage()->findAll('css', '#homebox div.homebox-portlet');
    if (empty($boxes) || count($boxes) < $count) {
      throw new Exception('Dashboard has only less than ' . $count . ' block' . ($count > 1 ? 's' : ''));
    }
  }

  /**
   * @Then /^I should see at least "([^"]*)" blocks in column "([^"]*)"$/
   */
  public function iShouldSeeAtLeastBlocksInColumn($count, $column)
  {
    // find dives with the class 'homebox-portlet' inside #homebox div
    $boxes = $this->getSession()->getPage()->findAll('css', '#homebox div.homebox-column-wrapper-' . $column . ' div.homebox-portlet');
    if (empty($boxes) || count($boxes) < $count) {
      throw new Exception('Column '. $column . ' has only less than ' . $count . ' block' . ($count > 1 ? 's' : ''));
    }
  }

  /**
  * @Then /^I should see "([^"]*)" items in block "([^"]*)"$/
  */
  public function iShouldSeeItemsInBlock($count, $block)
  {
    $obj_block = $this->getBlockInnerContainer($block);
    if (!empty($obj_block)) {
      $items = $obj_block->findAll('css', '.portlet-content > .item-list ul > li');
      if (empty($items) || count($items) < $count) {
        $message = 'The block: '. $block . ' has only less than ' . $count . ' item' . ($count > 1 ? 's' : '');
      }
    }else {
      $message = 'The block: '. $block . ' couldn\'t be found on Dashboard';
    }
    // if any errors
    if (isset($message))
      throw new Exception($message);
  }

  /**
   * @Then /^I should see the item "([^"]*)" in block "([^"]*)"$/
   */
  public function iShouldSeeTheItemInBlock($item, $block)
  {
    $obj_block = $this->getBlockInnerContainer($block);
    if (!empty($obj_block)) {
      $found = false;
      // get <li> tags in item-list div
      $lis = $obj_block->findAll('css', '.portlet-content > .item-list ul > li');
      if (!empty($lis)) {
        foreach ($lis as $li) {
          // check <li> text
          if ($item == $li->getText()) {
            $found = true;
            break;
          }
        }
      }
      if (!$found){
        //$message = 'The item: '. $item . ' cannot be found in block: ' . $block);
      }
    }else {
      $message = 'The block: '. $block . ' couldn\'t be found on Dashboard';
    }
    // if any errors
    if (isset($message))
      throw new Exception($message);
  }

  /**
  * Find dashboard block inner container div
  */
  private function getBlockInnerContainer($block) {
    $page = $this->getSession()->getPage();
    // find block with header    
    $blocks_h3 = $page->findAll('css', 'h3.portlet-header > span.portlet-title');
    if (!empty($blocks_h3)) {
      foreach ($blocks_h3 as $header_span) {
        if ($block == $header_span->getText()) {
          return $header_span->getParent()->getParent();
        }
      }
    }
    return null;
  }

  /**
   * @Then /^I drag the block "([^"]*)" onto "([^"]*)"$/
   */
  public function iDragTheBlockOnto($origin, $destination)
  {
    if ($draggable = $this->getBlockInnerContainer($origin)->getParent()) {
      $droppable = $this->getBlockInnerContainer($destination)->getParent()->getParent();
      if ($droppable) {
        $this->getSession()->wait(1, '');        
        $draggable->dragTo($droppable);
        $this->getSession()->wait(1, '');
      }else {
        $message = 'The block: ' . $destination . " cannot be found on Dashboard";
      }
    }else {
      $message = 'The block: ' . $origin . " cannot be found on Dashboard";
    }
    // if any errors
    if (isset($message))
      throw new Exception($message);
  }

  /**
   * @Then /^I drag the block "([^"]*)" onto column "([^"]*)"$/
   */
  public function iDragTheBlockOntoColumn($origin, $destination)
  {
    if ($draggable = $this->getBlockInnerContainer($origin)->getParent()) {
      $droppable = $this->getSession()->getPage()->find('css', '#homebox-column-'. $destination );
      if ($droppable) {
        $this->getSession()->wait(1, '');        
        $draggable->dragTo($droppable);
        $this->getSession()->wait(1, '');
      }else {
        $message = 'The column: ' . $destination . " cannot be found on Dashboard";
      }
    }else {
      $message = 'The block: ' . $origin . " cannot be found on Dashboard";
    }
    // if any errors
    if (isset($message))
      throw new Exception($message);
  }

  /**
   * @Then /^I should see the below <blocks> in column "([^"]*)"$/
  */
  public function iShouldSeeTheBelowBlocksInColumn($column, TableNode $table)
  {
    $table = $table->getHash();
    if (!empty($table)) {
      $page = $this->getSession()->getPage();
      // find block with header, for the column    
      $blocks_h3 = $page->findAll('css', '#homebox-column-' . $column . ' h3.portlet-header > span.portlet-title');    
      if (!empty($blocks_h3)) {
        $arr_boxes = array();
        foreach ($blocks_h3 as $header_span) {
          if ($boxname = $header_span->getText()) {
            $arr_boxes[] = $boxname;
          }
        }
        // check box exists
        if (!empty($arr_boxes)) {
          foreach ($table as $item) {
            // check the box exists in column boxes
            if (!in_array($item['blocks'], $arr_boxes)) {
              $message = 'The box: ' . $item['blocks'] .' cannot be found in column '. $column;
              break;
            }
          }
        }else {
          $message = 'The column '. $column . ' is empty';
        }
      }else {
        $message = 'The column '. $column . ' is empty';
      }
    }else {
      $message = 'Block list cannot be empty';
    }
    if (isset($message)) {
      throw new Exception($message);
    }
  }
  
  /**
   * @Then /^I should not see the below <blocks> in column "([^"]*)"$/
  */
  public function iShouldNotSeeTheBelowBlocksInColumn($column, TableNode $table)
  {
    $table = $table->getHash();
    if (!empty($table)) {
      $page = $this->getSession()->getPage();
      // find block with header, for the column    
      $blocks_h3 = $page->findAll('css', '#homebox-column-' . $column . ' h3.portlet-header > span.portlet-title');    
      if (!empty($blocks_h3)) {
        $arr_boxes = array();
        foreach ($blocks_h3 as $header_span) {
          if ($boxname = $header_span->getText()) {
            $arr_boxes[] = $boxname;
          }
        }
        // check box exists
        if (!empty($arr_boxes)) {
          foreach ($table as $item) {
            // check the box exists in column boxes
            if (in_array($item['blocks'], $arr_boxes)) {
              $message = 'The box: ' . $item['blocks'] .' is present in column '. $column;
              break;
            }
          }
        }else {
          $message = 'The column '. $column . ' is empty';
        }
      }else {
        $message = 'The column '. $column . ' is empty';
      }
    }else {
      $message = 'Block list cannot be empty';
    }
    if (isset($message)) {
      throw new Exception($message);
    }
  }
}
?>

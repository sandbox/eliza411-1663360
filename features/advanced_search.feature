Feature: Overall Filter Search for Issues
  In order to define the Advanced Filter search for Issues
  As an Authenticated user
  I wanted to look for Advanced Filter search for Issues
  
  Background:
    Given I am logged in as "site user"
    And I am on "/project/issues/search"
  
  Scenario: For visting the advanced search page
    And I should see the heading "Search issues for all projects"
    And I should see the following <texts>
    | texts |
    | Search for |
    | Project |
    | Assigned |
    | Submitted by |
    | Status |
    | Category |
    | Issue tags |
    When I fill in "Search for" with "Achievements"
    Then I should see at least "5" records
     
  @javascript
  Scenario: For seaching the project issue with submitted users
    When I fill in "Project" with "Achie"
    And I select "Achievements" from the suggestion "Project"
    And I press "Search" to filter
    And I wait for "1" seconds
    Then I should see at least "1" records
    And I should see the link "I'd like to back port this to D6 if no one is offended"
    And I follow "I'd like to back port this to D6 if no one is offended"
    And I should see the submitted user "gstout"
  
  @javascript 
  Scenario: For Search the issue with status/priority/category with additonal select
    When I fill in "Project" with "Achie"
    And I select "Achievements" from the suggestion "Project"
    And I wait for "2" seconds
    And I select the following <fields> with <values>
    | fields | values |
    | Status | active |
    | Status | needs review |
    | Priority | normal |
    | Priority | minor |
    And I press "Search" to filter
    Then I should see at least "2" records
  
  @javascript  
  Scenario: For Searching the project by applying all filters
    When I fill in "Project" with "Achie"
    And I select "Achievements" from the suggestion "Project"
    And I select the following <fields> with <values>
    | fields | values |
    | Status | active |
    | Status | needs review |
    | Status | closed (fixed)  |
    | Priority | normal |
    | Priority | minor |
    | Priority | major |
    | Category | feature request |
    | Category | support request |
    And I press "Search" to filter
    And I wait for "1" seconds
    Then I should see at least "2" records
  
  @javascript  
  Scenario: For Searching the issues with tags
    When I fill in "Assigned" with "sdb"
    And I select "sdboyer" from the suggestion "Assigned"    
    And I select "Is one of" from field "Issue tags"
    And I fill in "sprint 2" for "Issue tags"
    And I press "Search" to filter
    Then I should see at least "1" records
    And I fill in "Assigned" with "sdboyer"
    And I should see the link "Revisit syncEvent() logic on main repository sync plugin"
    And I follow "Revisit syncEvent() logic on main repository sync plugin"
    And I should see the link "sdboyer"
    
  @javascript
  Scenario: For Searching the issues with tags
    When I select "Is all of" from field "Issue tags"
    And I fill in "sprint 2, sprint 1" for "Issue tags"
    And I fill in "Assigned" with "mirzu"
    And I select "mirzu" from the suggestion "Assigned" 
    And I press "Search" to filter
    Then I should see the link "Port issue following functionality to D7"
    And I follow "Port issue following functionality to D7"
    Then I should see the following <links>
    | links |
    | sprint 2 |
    | sprint 1 |
Feature: Overall Filter Search for Issues
  In order to define the Advanced Filter search for Issues
  As an Authenticated user
  I wanted to look for Advanced Filter search for Issues

  Background:
    Given that I am on the homepage
    And I am logged in as "sachin2dhoni"
    Then I should see "Your Dashboard"
    And I follow "Your Dashboard"
    And I should see "Your Issues"
    And I follow "Your Issues"

  Scenario: For navigating on the user specific issues.
    And I should see the following <links>
    | links |
    | Create a new issue |
    | Advanced search |
    And I should see the following <texts>
    | texts |
    | Search for |
    | Project |
    | Status |
    | Priority |
    | Category |
    | Version |
    | Replies |
    | Last updated |
    | Assigned to |
    | Created |
    And I fill in "Search for" with "create issue"
    When I press user issue "search" to filter
    Then I should see at least "2" records

  @javascript
  Scenario: For Searching user specific issues.
    And I should see the following <links>
    | links |
    | Create a new issue |
    | Advanced search |
    When I fill in "Project" with "dr"
    And I select "Drupal.org Testing (.phar)" from the suggestion "Project"
    And I select the following <fields> with <values>
    | fields | values |
    | Status | fixed |
    | Priority | normal |
    And I wait for "5" seconds
    And I press user issue "search" to filter
    Then I should see at least "2" records
    And I should not see "No issues match your criteria"

  Scenario: For navigating through a specific project issue
    And I fill in "Search for" with "create issue"
    And I press user issue "search" to filter
    Then I should see at least "1" records
    And I follow "Write Feature For Creating a new issue (create_issues.feature)"
    And I should see the heading "Issue Summary"
    And I should see the heading "Comments"
    And I should see the heading "Post new comment"

  Scenario: For searching the records with priority with status/priority
    And I select the following <fields> with <values>
    | fields | values |
    | Status | fixed |
    | Priority | normal |
    And I press user issue "search" to filter
    Then I should see at least "1" records
    And I should see "fixed" under "Status"
    And I should see "normal" under "Priority"
    And I should not see "No issues match your criteria."
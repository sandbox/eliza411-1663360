@javascript
Feature:
  In order to test the functionalities of Drupal dashboard
  As an authenticated user
  I need to be able to check User Dashboard and its regions

  Background:
    Given I am on the homepage
    When I follow "Log in / Register"
    Then I should see the heading "User account"
    And I login to the site
    And I follow "Your Dashboard"
    
  Scenario: Check the links and labels on Dashboard
    When I follow "Dashboard"
    Then I should see the heading "pradeeprkara"
    And I should see the following <links>
    | links |
    | Dashboard |
    | Your Posts |
    | Your Commits |
    | Your Issues |
    | Your Projects |
    | Profile |
    | Add a block |
    | Make this your Homepage |

    Scenario: Check the regions on Dashboard
      When I follow "Dashboard"
      #Then I should see at least "20" blocks
      #Then I should see at least "3" blocks in column "2"
      #Then I should see "11" items in block "Planet Drupal"
      #Then I should see "10" items in block "Planet Drupal"
      #Then I should see the item "Write Feature File/s for Project issues" in block "Your Posts"
      #Then I drag the block "Drupal News" onto "Your Posts"
      #Then I should see at least "2" blocks in column "1"
      #Then I drag the block "Drupal News" onto column "3"
      #Then I should see the block "Tr" in position "20" in column "1"
    
    Scenario: Check blocks in a Column
      When I follow "Dashboard"
      Then I should see at least "2" blocks in column "1"
      And I should see the below <blocks> in column "1"
      | blocks |
      | Your Posts |
      | Drupal News |
      And I should not see the below <blocks> in column "1"
      | blocks |
      | Planet Drupal |
      | Track Posts |
@javascript
Feature: Search
In order to see a word definition
As a website user
I need to be able to search for a word

Background:
Given I am on "/project/issues"

Scenario: Searching for a page with autocompletion

When I fill in "projects" with "Bluemarine"
And I wait for the suggestion box to appear
Then I should see "Bluemarine"
And I should see "Bluemarine_smarty"
And I should see "Bluemarine ETS"
And I should see "Bluemarine Twig"
And I should not see "Bluefreedom"


Scenario: Searching for a page with autocompletion

When I fill in "projects" with "Bluema"
And I wait for the suggestion box to appear
Then I should see "Bluemarine"
And I should see "Bluemarine_smarty"
And I should see "Bluemarine ETS"
And I should see "Bluemasters"
And I should not see "Bluefreedom"


Scenario:Searching for a listed Entries
 
When I fill in "projects" with "Userda"
Then I wait for the suggestion box to appear
And I select "Userdashboard" from the suggestion
And I press search to filter
Then I should see "Issues"
And I wait "1000" seconds for the results to show
And I should see the link "UserDashboard"
And I should see the link "Multiple columns"
And I should see "closed"
And I select "active" from "edit-status"
And I press search to filter
And I wait "1000" seconds for the results to show
Then I should see the link "UserDashboard"
And I should see the link "Multiple columns"
And I should see "active"
And I should not see "needs working"
    

Scenario:Searching with all options

When I fill in "edit-text" with "project user signature"
And I fill in "projects" with "Mollom"
And I wait for the suggestion box to appear
And I select "Mollom" from the suggestion
And I select "active" from "edit-status"
And I select "normal" from "edit-priorities"
And I select "feature request" from "edit-categories"
And I press search to filter
And I wait "1000" seconds for the results to show
Then I should see the link "Mollom"
And I should see the link "Protect user signatures"
And I should not see "needs work"

 
Scenario: Login to drupal and should see the link "Create a new issue"

Given I am on "/user"
Then I login to the site
And I should see "Log out"
When I am on "/project/issues"
Then I should see "Create a new issue"
When I follow "Create a new issue"
When I press "Next"
Then I should see "You must select a project."
When I move backward one page
Then I should not see "Create a new issue"
#WORK IN PROGRESS
                   
        
  
  
  
    


 
  






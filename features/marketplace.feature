Feature: Find Drupal services
  In order to find the right Drupal service provider for me
  As any user
  I want to filter available providers by categories

  Scenario: Browse to the Marketplace preview page
    Given I am on the homepage
    When I follow "Marketplace"
    Then I should see "Marketplace preview"

  Scenario: See a paged list of service providers
    Given I am on "/drupal-services"
    When I follow "Marketplace preview"
    Then I should see "next ›"
    And I should see "Services"
    And I should see "Sectors"
    And I should see "Countries served"

  Scenario: See a filtered list of service providers
    Given I am on "/marketplace-preview" 
    When I follow "Consulting"
    And I follow "Government"
    Then I should see "Achieve Internet"
    And I should not see "2020Media"
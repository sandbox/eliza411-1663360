Feature: Add change record
  In order to see and add change records
  As an authenticated user
  I should be able to click on the respective link and submit the page

  Background:
    Given I am on "/project/drupal"
    When I follow "View change records"
    And I login to the site
    
  Scenario: Search records
    When I fill in "mail" for "Keywords"
    And I fill in "7.x" for "Introduced in branch"
    And I press "Apply"
    Then I should see at least "2" records

  Scenario: Add new change record
    When I follow "Add new change record"
    Then I should see "Create Change record"
    And I fill in the following:
    | Title | Test change record three |
    | Project | Drupal core |
    | Introduced in branch | 8.x |
    | Introduced in version | 8.x |
    | Description | Sundar test record three description goes here |
    And I check "Themers"
    And I check "Module developers"
    And I check "Theming guide done"
    And I enter "#1507: printable drupal documentation" for field "Issues"
    And I press "Save"
    Then I should see "Change record Test change record three has been created"
   
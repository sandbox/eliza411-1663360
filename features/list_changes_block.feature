Feature: Look for change record block in an issue
  In order to see the change record block
  As anonymous/guest user
  I should be able to see the block in the issue tagged to a change record

  Background:
    Given I am on "/project/drupal"
    When I follow "View change records"
    Then I should see the heading "Change records for Drupal core"

  Scenario: Navigate to the attached issue and look for the block
    When I follow "Test change record two"
    Then I should see "Posted by"
    And I should see "Sundar test record"
    When I follow "#1507: printable drupal documentation"
    Then I should see "Change records for this issue"
    And I should see the link "Test change record two"
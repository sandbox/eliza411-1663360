#@javascript
Feature:
  In order to test the functionalities of Drupal dashboard
  As an authenticated user
  I need to be able to check Your posts, Your issues, Your projects and Your commits
  
  Background: Log on to Drupal.org
    Given I am on the homepage
    When I follow "Log in / Register"
    Then I should see the heading "User account"
    When I login to the site
    And I follow "Your Dashboard"
  
  @javascript
  Scenario: Check Your Posts tab
    When I follow "Your Posts"
    Then I should see the link "Add Your Posts to dashboard"
    And I should see at least "2" records
    Then I check my posts
    
  Scenario: Check Your Commits Tab
    When I follow "Your Commits"
    Then I should see at least "2" records
    And I should see the following <links>
    | links |
    | Dashboard |
    | Your Posts |
    | Your Commits |
    | Your Issues |
    | Dashboard |
    | Your Projects |
    | Profile |
    
  Scenario: Check pagination link in Your Commits tab
    When I follow "Your Commits"
    And I click on page "next"
    Then I should see at least "2" records
    And I should see the following <links>
    | links |
    | first |
    | previous |
    
  Scenario: Check RSS feed in Your Commits tab
    When I follow "Your Commits"
    Then I click on the feed icon
    Then I should see at least "2" feed items
    And I should see the text "commit" in the feed
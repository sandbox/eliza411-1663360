Feature:
  In order to test the functionalities of Create Project
  As an Authenticated User
  I want to Create a Project
  
  Background:
    Given I am on the homepage
    And I am logged in as "git user"
    Then I follow "Your Dashboard"
    
  @javascript
  Scenario: Create a Project
    When I follow "Your Projects"
    Then I follow "Add a new project"
    Then I should see the heading "Create Project"
    And I select "Modules" from Project Type on Create Project page	
    And I select the following <fields> with <values>
    | fields | values |
    | Modules categories  | Event |
    | Maintenance status | Actively maintained |
    | Development status | Under active development |
    And I upload the following "project image" <files>
    | files | description | alt text |
    | desert.jpg | Desert pic | Desert |
    | hydrangeas.jpg | Hydrangeas pic | Hydrangeas |
    | koala.jpg | Koala pic | Koala |
    And I fill in "My First Sandbox" for "Project title:"
    And I fill in Project Description with "My Project description..."
    Then I follow "Project resources"
    And I fill in the following:
    | Homepage: | http://mytestsite.com |
    | Documentation: | docs here |
    | Screenshots: | screnshots... |
    | Changelog: | http://mytestsite.com |
    | Demo site: | http://mytestsite.com |
    And I fill in "Testing create project" for "Log message:"
    And I wait for "1" seconds    
    And I press "Save"
    And I wait for "1" seconds  
    Then I should see "The Git repository for this project has been enqueued for creation"
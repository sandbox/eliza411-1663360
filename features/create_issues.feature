Feature: Drupal Create Issues
  In order to add a Issues
  As an authenticated user
  I wanted to look for a link on the project issues page

  Scenario: Login to drupal and should see the link "Create a new issue"
    Given I am on "/project/issues"
    When I login to the site
    And I follow "Create a new issue"
    And I select "Achievements" from "Project"
    And I press "Next"
    And I should see the following <texts>
    | texts |
    | Version |
    | Component |
    | Category |
    | Title |
    | Description |
    And I select the following <fields> with <values>
    | fields | values |
    | Version | 7.x-1.4 |
    | Component | Code |
    | Category | task |
    | Priority | normal |
    | Assigned | site user |
    | Status | needs work |
    And I fill in the following:
    | Title | Need to Check D7 Version -test8|
    | Description | Sample description as part of the testing environment.|
    And I should see the link "More information about formatting options"
    And I attach the file "/root/Desktop/Screenshot.jpg" to "Attach new file"
    And I press "Save"
    Then I should see the heading "Need to Check D7 Version -test8"